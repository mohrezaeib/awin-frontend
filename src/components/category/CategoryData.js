export const img = [
  {
    author: "The Lazy Artist Gallery",
    id: 1,
    tag: "People",
    img: "https://cdn.pixabay.com/photo/2013/07/02/22/20/bouquet-142876_960_720.jpg",
  },
  {
    id: 2,
    author: "Daria Shevtsova",
    tag: "Food",
    img: "https://cdn.pixabay.com/photo/2017/05/25/09/22/flower-2342706_960_720.jpg",
  },
  {
    id: 3,
    author: "Kasuma",
    tag: "Animals",
    img: "https://cdn.pixabay.com/photo/2015/04/19/08/32/rose-729509_960_720.jpg",
  },
  {
    id: 1,
    author: "Dominika Roseclay",
    tag: "Plants",
    img: "https://cdn.pixabay.com/photo/2018/10/30/16/06/water-lily-3784022_960_720.jpg",
  },
  {
    id: 2,
    author: "Scott Webb",
    tag: "Plants",
    img: "https://github.com/OlgaKoplik/CodePen/blob/master/filterGallery/5.jpg?raw=true",
  },
  {
    id: 1,
    author: "Jeffrey Czum",
    tag: "People",
    img: "https://cdn.pixabay.com/photo/2015/10/09/00/55/lotus-978659_960_720.jpg",
  },
  {
    id: 3,
    author: "Dominika Roseclay",
    tag: "Food",
    img: "https://cdn.pixabay.com/photo/2015/04/19/08/32/rose-729509_960_720.jpg",
  },
  {
    id: 3,
    author: "Valeria Boltneva",
    tag: "Animals",
    img: "https://cdn.pixabay.com/photo/2012/03/01/00/55/garden-19830_960_720.jpg",
  },
];
